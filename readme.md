flisol new logo
https://raw.githubusercontent.com/FlisolCaba/graficas/master/logos/flisol%20magenta.svg
https://github.com/FlisolCaba/graficas

debian logo
https://www.debian.org/logos/openlogo-nd.svg
https://www.debian.org/logos/

linuxmint logo
https://upload.wikimedia.org/wikipedia/commons/3/3f/Linux_Mint_logo_without_wordmark.svg
https://commons.wikimedia.org/wiki/File:Linux_Mint_logo_without_wordmark.svg

ubuntu mate logo
https://raw.githubusercontent.com/ubuntu-mate/brand-artwork/master/ubuntu-mate/Ubuntu-MATE-Rondel-Green.svg
https://ubuntu-mate.org/logo-guidelines/

ubuntu logo
https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo-ubuntu_cof-orange-hex.svg
https://commons.wikimedia.org/wiki/File:Logo-ubuntu_cof-orange-hex.svg
https://design.ubuntu.com/downloads/

manjaro logo
https://upload.wikimedia.org/wikipedia/commons/3/3e/Manjaro-logo.svg
https://commons.wikimedia.org/wiki/File:Manjaro-logo.svg

archlinux logo
https://www.archlinux.org/static/logos/archlinux-logo-dark-scalable.518881f04ca9.svg
https://www.archlinux.org/art/

firefox logo
https://design.firefox.com/product-identity/firefox/firefox/firefox-logo.svg
https://design.firefox.com/photon/visuals/product-identity-assets.html

docker logo
https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg
https://commons.wikimedia.org/wiki/File:Docker_(container_engine)_logo.svg

git logo
https://git-scm.com/images/logos/downloads/Git-Icon-1788C.eps
https://git-scm.com/downloads/logos

tux
https://upload.wikimedia.org/wikipedia/commons/2/2b/Tux-simple.svg
https://en.wikipedia.org/wiki/File:Tux-simple.svg